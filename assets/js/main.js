jQuery(document).ready(function($) {

    //getting theme directory from WordPress
    var templateDir = faces_localized_data.templateDir;

	/* Expand and Collapse */
	$('.mem-testimonials').click(function() {
		if ( $('.mem-testimonials-content').is(':visible') ) {
			$('.mem-testimonials').css('background', '#f2f2f1 url('+templateDir+'/assets/images/expand.png) 607px -58px no-repeat');
			$('.mem-testimonials-collapse').html('expand');
			$('.mem-testimonials-content').slideUp("slow");
		} else {
			$('.mem-testimonials').css('background', '#fadfd6 url('+templateDir+'/assets/images/expand.png) 607px 10px no-repeat');
			$('.mem-testimonials-collapse').html('collapse');
			$('.mem-testimonials-content').slideDown("slow");
		}
	});
	
	/* Linkedin Coming */
	$('.mem-linked-link').click(function() {
		if ( $('#linked-coming').is(':visible') ) {
			$('#linked-coming').slideUp("slow");
		} else {
			$('#linked-coming').slideDown("slow");
		}
	});
	
	/* Referal Form */
	$('.btn-referme').click(function() {
		if ( $('#referme-form').is(':visible') ) {
			$('#referme-form').slideUp("slow");
		} else {
			$('#referme-form').slideDown("slow");
		}
	});
	
	/* Mobile Menus */
	$('#menu-btn-mobile').click(function() {
		if ( $('#menu-display-mobile').is(':visible') ) {
			$('#menu-display-mobile').slideUp("slow");
		} else {
			$('#menu-display-mobile').slideDown("slow");
		}
	});
	/* add submenu icon */
	if ($('#menu-display-mobile .menu-item').find('.sub-menu').length > 0) {
		$(".sub-menu").parent('#menu-display-mobile .menu-item').css('background', '#494A52 url('+templateDir+'/assets/images/submenu-icon-mobile.gif) 99% 11px no-repeat');
		$(".sub-menu").siblings('#menu-display-mobile a').css('margin-right', '39px');
		$(".sub-menu").parent('#menu-display-mobile .menu-item').append( ( "<div class='submenu-expand'></div>" ) );
	}
	/* expand submenus */
	$('.submenu-expand').click(function() {
		if ( $(this).siblings('.sub-menu').is(':visible') ) {
			$(this).siblings('.sub-menu').slideUp('slow');
		} else {
			$(this).siblings('.sub-menu').slideDown('slow');
		}
	});
	
	/* Mosaic */
	$('.bar').mosaic({
		animation	:	'slide',		//fade or slide
	});
	$('.cover2').mosaic({
		animation	:	'slide',	//fade or slide
		anchor_y	:	'top',		//Vertical anchor position
		hover_y		:	'200px'		//Vertical position on hover
	});
	// on mobile devices, enable the hover on click
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
		$('.mainphoto-link').click(function(e) {
			if($(this).hasClass( "link-disabled" )) {
				// link enabled
			} else {
				// link disabled
				e.preventDefault();
				$( "a" ).removeClass( "link-disabled" );
				$( this ).addClass( "link-disabled" );
			}
		});
	}
});