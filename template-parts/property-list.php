<?php

// the query
$args = array(
        'post_type' => 'estate_property',
        'posts_per_page' => 6
    );
$the_query = new WP_Query( $args ); ?>

<?php if ( $the_query->have_posts() ) : ?>
	
    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

    	<div class="waynakh-property-list__property">
    		<header class="waynakh-property-list__property__header">
    			<div class="img">
    				<?php
    					if(has_post_thumbnail()){
    						the_post_thumbnail('property-grid-thumb');
    					}
    				?>
    			</div>
    			<div class="info">
    				<h3><?php the_title(); ?></h3>
    				<p class="location">Location, Location</p>
    			</div>
    		</header>
    		<section class="description">
    			<div class="price-review">
    				<div class="price">350<span>TL</span></div>
    				<p class="misc">Misc note here</p>
    				<div class="reviews">92</div>
    			</div>
    			<div class="capacity">
    				<div class="persons">
    					<?php 
    						echo get_post_meta( get_the_ID(), 'property_size', true );
    					?>
    				</div>
    				<div class="bed-rooms">
    					<?php 
    						echo get_post_meta( get_the_ID(), 'property_bedrooms', true );
    					?>
    				</div>
    				<div class="bath-rooms">
    					<?php 
    						echo get_post_meta( get_the_ID(), 'property_bathrooms', true );
    					?>
    				</div>
    			</div>			
    		</section>

    	</div>
    <?php endwhile; ?>
    <?php wp_reset_postdata(); ?>

<?php else : ?>
    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>