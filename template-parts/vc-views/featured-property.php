<?php
/**
 * Shows single block of featured property
 *
 * 
 */
?>

<?php
//all values
$id = get_the_ID();
$name = get_the_title();
$city = wp_get_post_terms($id, 'property_city', array("fields" => "all"))[0];
$city_url = get_term_link($city->term_id);
$area = wp_get_post_terms($id, 'property_area', array("fields" => "all"))[0];
$area_url = get_term_link($area->term_id);
$guests = get_post_meta( $id, 'guest_no', true );
$bedrooms = get_post_meta( $id, 'property_bedrooms', true );
$bathrooms = get_post_meta( $id, 'property_bathrooms', true );
$price = get_post_meta( $id, 'property_price', true );
$property_rating = waynakh_property_rating_number($id);
?>

<article class="featured-properties__property">
<div class="featured-properties__property-inner">

	<header>
		<div class="featured-properties__property-image">
			<a href="<?php the_permalink(); ?>">
			<?php
				if(has_post_thumbnail()){
					the_post_thumbnail( 'property-grid-thumb');
				}
			?>
			</a>
		</div>
		<div class="featured-properties__property-heading">
			<h2><a href="<?php the_permalink(); ?>"><?php echo $name; ?></a></h2>
			<h3>
				<a href="<?php echo $area_url; ?>"><?php echo $area->name; ?></a> | 

				<a href="<?php echo $city_url; ?>"><?php echo $city->name; ?></a></h3>
		</div>		
	</header>

	<section class="featured-properties__property-info">
		<div class="row">
			<div class="featured-properties__property-price column col-xs-4">
				<span class="head"><?php _e('Night', 'waynakh-theme'); ?></span>
				<span class="amount"><?php echo $price; ?>,</span>
				<span class="amount-ext">00</span>
				<span class="currency">TL</span>
				<span class="note"><?php _e('Prices starting from', 'waynakh-theme'); ?></span>
			</div>

			<div class="featured-properties__property-point column col-xs-2">
				<span class="text"><?php _e('Point', 'waynakh-theme'); ?></span>
				<span class="number">
					<?php echo $property_rating; ?>
				</span>
			</div>
		
			<div class="column col-xs-6 clearfix">
				<div class="featured-properties__property-guests">
					<div class="inner">
						<?php echo $guests; ?>
					</div>
				</div>
				<div class="featured-properties__property-bedrooms">
					<div class="inner">
						<?php echo $bedrooms; ?>
					</div>
				</div>
				<div class="featured-properties__property-bathrooms">
					<div class="inner">
						<?php echo $bathrooms; ?>
					</div>
				</div>
			</div>
		</div>			
	</section>
</div>
</article>