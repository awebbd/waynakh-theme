<?php
/**
 *	Villa Search View
 *
 *
 *
 *
 * 
 * 
 */
?>

<?php
/**
 * Important Values & Variables
 * 
 */
$adv_submit = wpestate_get_adv_search_link();
?>

<div class="villa-search clearfix">
	<form  method="get"  id="main_search" action="<?php print $adv_submit; ?>" >
		<div class="row">
			<div class="col-md-4 col--has-right-border villa-search__field">
				<div class="advanced-city-container map_icon">
					<input type="text" id="advanced_city"      class="form-control" name="advanced_city" data-value=""   value="" placeholder="Where do you want to go ?" autocomplete="off">
					<div class="advanced-city-suggestions" style="display: none;">
						<div class="__inner-wrap">

							<div class="__ajax-content">

							<?php
								$cities = get_terms(array('taxonomy'=> 'property_city', 'number'=> 3));
							?>

							<?php if ( ! empty( $cities ) && ! is_wp_error( $cities ) ) : ?>

								<?php foreach ($cities as $city) : ?>

								<a class="__suggestion clearfix">
									<div class="__suggestion-location">
										<p class="__name"><?php echo $city->name; ?></p>
									</div>
									<div class="__suggestion-villa-count">
										<div class="btn btn--rounded">
											<?php echo $city->count; ?> Villa
										</div>
									</div>
								</a>

								<?php endforeach; ?>

							<?php else: ?>

								<small>Nothings found.</small>

							<?php endif; ?>

							</div>

							<div class="please-wait" style="display: none;">
								searching...
							</div>
													
						</div>
					</div><!--.advanced-city-suggestions-->
				</div><!--.advanced-city-container-->
			</div>
			<div class="col-md-4 col--has-right-border">
				<div class="row">
					<div class="col-md-6 has_calendar calendar_icon __check-in villa-search__field">
						<input type="text" id="checkinshortcode" class="form-control" name="check_in" placeholder="Check in">
					</div>
					<div class="col-md-6 has_calendar calendar_icon checkout_sh __check-out villa-search__field">
						<input type="text" id="checkoutshortcode" disabled="" class="form-control" name="check_out" placeholder="Check Out">
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="row">
					<div class="col-md-6 villa-search__field">
						<div class="input-number--with-buttons clearfix">
							<input type="text" id="guests" class="form-control hasPlusMinusButtons" name="guest_no" value="0">

							<label for="guests">Misafir</label>

							<span class="btn btn--minus">-</span>
							<span class="btn btn--plus">+</span>
						</div>						
					</div>
					<div class="col-md-6 villa-search__submit">
						<input name="submit" type="submit" class="btn btn--green btn--rounded btn--small btn--no-shadow btn--has-arrow" id="advanced_submit_2" value="<?php esc_html_e('Villa Ara','wpestate');?>">
					</div>
				</div>
			</div>
		</div>
	</form>
</div><!--/.villa-search-->