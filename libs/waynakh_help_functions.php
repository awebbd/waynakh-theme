<?php 
////////////////////////////////////////////////////////////////////////////////
/// show hieracy categ
////////////////////////////////////////////////////////////////////////////////
if( !function_exists('wpestate_get_category_select_list') ):
    /*function wpestate_get_category_select_list($args){
        $taxonomy           =   'property_category';
        $categories         =   get_terms($taxonomy,$args);
        $categ_select_list  =  '<li role="presentation" data-value="all">'. esc_html__( 'All Types','wpestate').'</li>'; 

        foreach ($categories as $categ) {
            $categ_select_list     .=   '<li role="presentation" data-value="'.$categ->slug.'">'. ucwords ( urldecode( $categ->name ) ).' ('.$categ->count.')'.'</li>';
            $categ_select_list     .=   wpestate_hierarchical_category_childen($taxonomy, $categ->term_id,$args );    
        }
        return $categ_select_list;
    }*/
     function wpestate_get_category_select_list($args){
        $taxonomy           =   'property_category';
        $categories         =   get_terms($taxonomy,$args);
      
        $categ_select_list  =  '<li role="presentation" data-value="'.$categ->slug.'"><label for="f-'.$categ->slug.'"><input type="radio" id="f-'.$categ->slug.'" name="selector"> '. __('All Types','wpestate').'</label></li>'; 

        foreach ($categories as $categ) {
            $counter = $categ->count;
            $received = wpestate_hierarchical_category_childen($taxonomy, $categ->term_id,$args ); 
         
        
              
            if(isset($received['count'])){
                $counter = $counter+$received['count'];
            }
            
           // $categ_select_list     .=   '<li role="presentation" data-value="'.$categ->slug.'">'. ucwords ( urldecode( $categ->name ) ).' ('.$counter.')'.'</li>';

            $categ_select_list     .=   '<li role="presentation" data-value="'.$categ->slug.'"><label for="f-'.$categ->slug.'"><input type="radio" id="f-'.$categ->slug.'" name="selector"> '. ucwords ( urldecode( $categ->name ) ).' ('.$counter.')'.'</label></li>';
            if(isset($received['html'])){
                $categ_select_list     .=   $received['html'];  
            }
            
        }
        return $categ_select_list;
    }
endif;

////////////////////////////////////////////////////////////////////////////////
/// show hieracy area
////////////////////////////////////////////////////////////////////////////////
if( !function_exists('wpestate_get_guest_dropdown') ):
    function wpestate_get_guest_dropdown($with_any='',$selected=''){
        $select_area_list='';
        if($with_any==''){
            $select_area_list.='<li role="presentation" data-value="0">'.esc_html__( 'any','wpestate').'</li>';
        }
        
        $select_area_list .=   '<li role="presentation" data-value="1"'; 
        if($selected==1){
            $select_area_list .=' selected="selected" ';
        }
        $select_area_list .= '>1 '.esc_html__( 'guest','wpestate').'</li>';
        
        $guest_dropdown_no                    =   intval   ( get_option('wp_estate_guest_dropdown_no','') );
        for($i=2;$i<=$guest_dropdown_no;$i++){
            $select_area_list .=   '<li role="presentation" data-value="'. $i.'"';
            if($selected!='' && $selected==$i){
                $select_area_list .=' selected="selected" ';
            }
            $select_area_list .= '>'. $i.' '.esc_html__( 'guests','wpestate').'</li>';
        }

        return $select_area_list;
    }
endif;

///////////////////////////////////////////////////////////////////////////////////////////
// List features and ammenities
///////////////////////////////////////////////////////////////////////////////////////////

if( !function_exists('estate_listing_features') ):
    function estate_listing_features($post_id){
        $return_string='';    
        $counter            =   0;                          
        $feature_list_array =   array();
        $feature_list       =   esc_html( get_option('wp_estate_feature_list') );
        $feature_list_array =   explode( ',',$feature_list);
        $total_features     =   round( count( $feature_list_array )/2 );


         $show_no_features= esc_html ( get_option('wp_estate_show_no_features','') );



            if($show_no_features!='no'){
                foreach($feature_list_array as $checker => $value){
                        $counter++;
                        $post_var_name  =   str_replace(' ','_', trim($value) );
                        $input_name     =   wpestate_limit45(sanitize_title( $post_var_name ));
                        $input_name     =   sanitize_key($input_name);


                        if (function_exists('icl_translate') ){
                            $value     =   icl_translate('wpestate','wp_estate_property_custom_amm_'.$value, $value ) ;                                      
                        }
                        $value= stripslashes($value);

                        if (esc_html( get_post_meta($post_id, $input_name, true) ) == 1) {
                             $return_string .= '<div class="listing_detail col-md-4"><i class="fa fa-check checkon"></i>' . trim($value) . '</div>';
                        }else{
                            $return_string  .=  '<div class="listing_detail not_present col-md-4"><i class="fa fa-times"></i>' . trim($value). '</div>';
                        }
                  }
            }else{

                foreach($feature_list_array as $checker => $value){
                    $post_var_name  =  str_replace(' ','_', trim($value) );
                    $input_name     =   wpestate_limit45(sanitize_title( $post_var_name ));
                    $input_name     =   sanitize_key($input_name);

                    if (function_exists('icl_translate') ){
                        $value     =   icl_translate('wpestate','wp_estate_property_custom_amm_'.$value, $value ) ;                                      
                    }

                    if (esc_html( get_post_meta($post_id, $input_name, true) ) == 1) {
                        $return_string .=  '<div class="listing_detail col-md-6"><i class="fa fa-check checkon"></i>' . trim($value) . '</div>';
                    }
                }

           }

        return $return_string;
    }
endif; // end   estate_listing_features  

///////////////////////////////////////////////////////////////////////////////////////////
// custom details
///////////////////////////////////////////////////////////////////////////////////////////
if( !function_exists('aweb_wpestate_show_custom_details') ):
    function aweb_wpestate_show_custom_details($edit_id,$is_dash=0){
        $week_days=array(
            '0'=>esc_html__('All','wpestate'),
            '1'=>esc_html__('Monday','wpestate'), 
            '2'=>esc_html__('Tuesday','wpestate'),
            '3'=>esc_html__('Wednesday','wpestate'),
            '4'=>esc_html__('Thursday','wpestate'),
            '5'=>esc_html__('Friday','wpestate'),
            '6'=>esc_html__('Saturday','wpestate'),
            '7'=>esc_html__('Sunday','wpestate')

            );
        $price_per_guest_from_one       =   floatval   ( get_post_meta($edit_id, 'price_per_guest_from_one', true) );
     
        $currency                       = esc_html( get_option('wp_estate_currency_label_main', '') );
        $where_currency                 = esc_html( get_option('wp_estate_where_currency_symbol', '') );
        
        $mega           =   wpml_mega_details_adjust($edit_id);
        $price_array    =   wpml_custom_price_adjust($edit_id);
        
        
        if (empty($mega) && empty($price_array)){
            return;
        }
        
        
        if(is_array($mega)){
            // sort arry by key
            ksort($mega);
        

            $flag=0;
            $flag_price         ='';
            $flag_min_days      ='';
            $flag_guest         ='';
            $flag_price_week    ='';
            $flag_change_over   ='';
            $flag_checkout_over ='';


            print '
            <thead class="custom_day_header"> <tr>
                <th scope="col" class="aweb_custom_day_from_to">'.esc_html__('Period','wpestate').'</th>';
                
                if($price_per_guest_from_one!=1){
                    print'
                    <th scope="col" class="aweb_custom_price_per_day">'.esc_html__('Price per night','wpestate').'</th>     
                    <th scope="col" class="aweb_custom_day_min_days">'.esc_html__('Minimum Booking days','wpestate').'</th> ';
                }else{
                    //print '<div class="custom_day_name_price_per_guest">'.esc_html__('Price per guest','wpestate').'</div>';
                }
            
             
                
            print'</tr></thead><tbody>';  
            
          //print_r($mega);
            foreach ($mega as $day=>$data_day){          
                $checker            =   0;
                $from_date          =   new DateTime("@".$day);
                $to_date            =   new DateTime("@".$day);
                $tomorrrow_date     =   new DateTime("@".$day);
                
                $tomorrrow_date->modify('tomorrow');
                $tomorrrow_date     =   $tomorrrow_date->getTimestamp();
               
                //we set the flags
                //////////////////////////////////////////////////////////////////////////////////////////////
                if ($flag==0){
                    $flag=1;
                    if(isset($price_array[$day])){
                        $flag_price         =   $price_array[$day];
                    }
                    $flag_min_days                  =   $data_day['period_min_days_booking'];
                    $flag_guest                     =   $data_day['period_extra_price_per_guest'];
                    $flag_price_week                =   $data_day['period_price_per_weekeend'];
                    $flag_change_over               =   $data_day['period_checkin_change_over'];
                    $flag_checkout_over             =   $data_day['period_checkin_checkout_change_over'];
                    
                    if(isset(  $data_day['period_price_per_month'])){
                        $flag_period_price_per_month    =   $data_day['period_price_per_month'];
                    }
                    
                    if(isset(  $data_day['period_price_per_week'])){
                        $flag_period_price_per_week     =   $data_day['period_price_per_week'];
                    }
                    
                    $from_date_unix     =   $from_date->getTimestamp();
                    print' <tr class="aweb_custom_day">';
                    print' <td class="aweb_custom_day_from_to"> '.esc_html__('','wpestate').' '. $from_date->format('d M');
                }

                
                
    
                //we check period chane
                //////////////////////////////////////////////////////////////////////////////////////////////
                if ( !array_key_exists ($tomorrrow_date,$mega) ){ // non consecutive days
                    $checker = 1; 
                 
                }else {
                    if( isset($price_array[$tomorrrow_date]) && $flag_price!=$price_array[$tomorrrow_date] ){
                        // IF PRICE DIFFRES FROM DAY TO DAY
                        $checker = 1;     
                    }
                    if( $mega[$tomorrrow_date]['period_min_days_booking']                   !=  $flag_min_days || 
                        $mega[$tomorrrow_date]['period_extra_price_per_guest']              !=  $flag_guest || 
                        $mega[$tomorrrow_date]['period_price_per_weekeend']                 !=  $flag_price_week || 
                        ( isset( $mega[$tomorrrow_date]['period_price_per_month'] ) && $mega[$tomorrrow_date]['period_price_per_month']                    !=  $flag_period_price_per_month ) || 
                        ( isset( $mega[$tomorrrow_date]['period_price_per_week'] ) && $mega[$tomorrrow_date]['period_price_per_week']                     !=  $flag_period_price_per_week ) || 
                        $mega[$tomorrrow_date]['period_checkin_change_over']                !=  $flag_change_over ||  
                        $mega[$tomorrrow_date]['period_checkin_checkout_change_over']       !=  $flag_checkout_over){
                            // IF SOME DATA DIFFRES FROM DAY TO DAY
                       
                            $checker = 1;
                        } 

                }

                if (  $checker == 0 ){
                    // we have consecutive days, data stays the sa,e- do not print 
                } else{
                    // no consecutive days - we CONSIDER print


                        if($flag==1){
                           
                         //   $to_date->modify('yesterday');
                            $to_date_unix     =   $from_date->getTimestamp();
                            print ' '.esc_html__(' - ','wpestate').' '. $from_date->format('d M Y').'</td>';
                           
                            if($price_per_guest_from_one!=1){
                                print'
                                <td class="aweb_custom_price_per_day">';
                                if( isset($price_array[$day]) ){
                                    echo   wpestate_show_price_booking($price_array[$day],$currency,$where_currency,1);
                                }else{
                                    echo '-';
                                }
                                print'</td>';

                                print'
                                <td class="aweb_custom_day_min_days">';
                                if( $flag_min_days!=0 ){
                                    echo $flag_min_days.esc_html__(' Days ','wpestate');
                                }else{
                                    echo '-';
                                }
                          
                                print '</td>';
                               
                                
                            }else{
                               
                            }
                            
                         
                            
                            if($is_dash==1){
                                print '<div class="delete delete_custom_period" data-editid="'.$edit_id.'" data-fromdate="'.$from_date_unix.'" data-todate="'.$to_date_unix.'"><a href="#"> '.esc_html__('delete period','wpestate').'</a></div>';
                            }
                            
                            print '</tr>'; 
                        }
                        $flag=0;
                        if( isset( $price_array[$day])){
                            $flag_price         =   $price_array[$day];
                        }
                        $flag_min_days      =   $data_day['period_min_days_booking'];
                        $flag_guest         =   $data_day['period_extra_price_per_guest'];
                        $flag_price_week    =   $data_day['period_price_per_weekeend'];
                        $flag_change_over   =   $data_day['period_checkin_change_over'];
                        $flag_checkout_over =   $data_day['period_checkin_change_over'];
                }
            }
            print '</tbody>';
            
        }
    }
endif;    