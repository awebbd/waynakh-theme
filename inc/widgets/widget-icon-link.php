<?php
/**
 * Widget Icon Link
 *
 * This widget shows a link 
 * with a icon selected.
 */
class Icon_Link_Widget extends WP_Widget
{
	/**
     * Register widget with WordPress.
     */
    function __construct(){
        parent::__construct(
            'icon_link_widget',
            'Icon Link Widget',
            array(
                'description' => 'A widget to show link with icon.'
            )
        );
    }

    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args     Widget arguments.
     * @param array $instance Saved values from database.
     */
    public function widget( $args, $instance ) { ?>

    		<?php echo $args['before_widget']; ?>
				<a 
					class="icon-link" 
					href="<?php echo $instance['url']; ?>">
					<?php echo $instance['title']; ?>
                    <i class="icon icon--small icon--<?php echo $instance['icon']; ?>"></i>
				</a>

    		<?php echo $args['after_widget']; ?>


    <?php
    }

    /**
     * Back-end widget form.
     *
     * @see WP_Widget::form()
     *
     * @param array $instance Previously saved values from database.
     */
    public function form( $instance ) {
        $title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( 'Link title', 'waynakh-theme' );

        $url = ! empty( $instance['url'] ) ? $instance['url'] :  esc_url( 'http://www.example.com' );

        $icon = ! empty( $instance['icon'] ) ? $instance['icon'] : esc_html__( 'sun-sea', 'waynakh-theme' );



        ?>
        <p>
        <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_attr_e( 'Title:', 'waynakh-theme' ); ?></label> 
        <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
        </p>

        <p>
        <label for="<?php echo esc_attr( $this->get_field_id( 'url' ) ); ?>"><?php esc_attr_e( 'URL:', 'waynakh-theme' ); ?></label> 
        <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'url' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'url' ) ); ?>" type="text" value="<?php echo esc_attr( $url ); ?>">
        </p>

        <p>
        <label for="<?php echo esc_attr( $this->get_field_id( 'icon' ) ); ?>"><?php esc_attr_e( 'Icon:', 'waynakh-theme' ); ?></label> 
        <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'icon' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'icon' ) ); ?>" type="text" value="<?php echo esc_attr( $icon ); ?>">
        </p>




        <?php 
    }

    /**
     * Sanitize widget form values as they are saved.
     *
     * @see WP_Widget::update()
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     */
    public function update( $new_instance, $old_instance ) {
       $instance = array();

       $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

       $instance['url'] = ( ! empty( $new_instance['url'] ) ) ? strip_tags( $new_instance['url'] ) : '';

       $instance['icon'] = ( ! empty( $new_instance['icon'] ) ) ? strip_tags( $new_instance['icon'] ) : '';

       return $instance;


    }






}
//hook into widget_init and register widget
function register_icon_link_widget(){
	register_widget( 'Icon_Link_Widget' );
}
add_action( 'widgets_init', 'register_icon_link_widget');
