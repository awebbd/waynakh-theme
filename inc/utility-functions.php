<?php
/**
 * Utility functions
 *
 * These functions helps in development of
 * other features.
 */

/**
 * Returns Property Rating Number
 *
 * 
 */
function waynakh_property_rating_number($post_id){

	$comments_count = wp_count_comments($post_id);

	if ( $comments_count->total_comments > 0 ){

		$rating_json = wpestate_calculate_property_rating( $post_id );
		$rating = json_decode( $rating_json );

		return $rating->rating; 

	}else{
		return 0;
	}
}

/**
 * Modified Listing Features
 * 
 */
if( !function_exists('waynakh_estate_listing_features') ):
    function waynakh_estate_listing_features($post_id){
        $return_string='';    
        $counter            =   0;                          
        $feature_list_array =   array();
        $feature_list       =   esc_html( get_option('wp_estate_feature_list') );
        $feature_list_array =   explode( ',',$feature_list);
        $total_features     =   round( count( $feature_list_array )/2 );


         $show_no_features= esc_html ( get_option('wp_estate_show_no_features','') );



            if($show_no_features!='no'){
            	$counter = 1;
                foreach($feature_list_array as $checker => $value){
                        $counter++;
                        $post_var_name  =   str_replace(' ','_', trim($value) );
                        $input_name     =   wpestate_limit45(sanitize_title( $post_var_name ));
                        $input_name     =   sanitize_key($input_name);


                        if (function_exists('icl_translate') ){
                            $value     =   icl_translate('wpestate','wp_estate_property_custom_amm_'.$value, $value ) ;                                      
                        }
                        $value= stripslashes($value);

                        if (esc_html( get_post_meta($post_id, $input_name, true) ) == 1) {
                             $return_string .= '<div class="listing_detail col-md-4"><i class="fa fa-check checkon"></i>' . trim($value) . '</div>';
                        }else{
                        	//don't show any value if not checked
                        }
                  }
            }else{

                foreach($feature_list_array as $checker => $value){
                    $post_var_name  =  str_replace(' ','_', trim($value) );
                    $input_name     =   wpestate_limit45(sanitize_title( $post_var_name ));
                    $input_name     =   sanitize_key($input_name);

                    if (function_exists('icl_translate') ){
                        $value     =   icl_translate('wpestate','wp_estate_property_custom_amm_'.$value, $value ) ;                                      
                    }

                    if (esc_html( get_post_meta($post_id, $input_name, true) ) == 1) {
                        $return_string .=  '<div class="listing_detail col-md-4"><i class="fa fa-check checkon"></i>' . trim($value) . '</div>';
                    }
                }

           }

        return $return_string;
    }
endif; // end   waynakh_estate_listing_features 



/**
 * Displays a dropdown of
 * provided taxonomy terms
 *
 * @return  string Dropdown of terms of taxonomy.
 */
function aweb_wp_term_dropdown($taxonomy){ ?>

    <select name="taxonomy" id="taxonomy" class="postform">
    <option value="-1">Choose one...</option>
    <?php
    $terms = get_terms($taxonomy);
    foreach ($terms as $term) {
        printf( '<option class="level-0" value="%s">%s</option>', $term->slug, $term->name );
    }
    echo '</select>';
    ?>

<?php
}