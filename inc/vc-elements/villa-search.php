<?php
/**
 * Villa Search
 *
 * Shows a custom search box with customized options and
 * views that searches properties listed on the site.
 * 
 */
class VillaSearchShortcode extends WPBakeryShortCode
{
	/**
	 * Init element with VisualComposer
	 * 
	 */
    function __construct() 
    {
        add_action( 'init', array( $this, 'villa_search_shortcode_mapping' ) );
        add_shortcode( 'villa_search_shortcode', array( $this, 'villa_search_shortcode_output' ) );
    }

    /**
     * Maps the shortcode with visual composer and
     * adds opiton to edit on backend.
     *
     *
     * 
     */
    public function villa_search_shortcode_mapping()
    {

    	// Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }

        // Map the block with vc_map()
        vc_map( 
            array(
                'name' => __('Villa Search', 'waynakh-theme'),
                'base' => 'villa_search_shortcode',
                'description' => __('An advanced search module for villas.'), 
                'category' => __('Waynakh Theme Shortcodes', 'waynakh-theme'),   
                'icon' => get_stylesheet_directory_uri().'/assets/images/vc-icon-villa.png'
            )
        ); 
    }

    /**
     * Outputs the shortcode 
     *
     *
     * 
     */
    public function villa_search_shortcode_output( $atts )
    {
    	
        ob_start(); ?>

        <div class="villa-search-container">

        	<?php get_template_part('template-parts/vc-views/villa', 'search' ); ?>

        </div>       	

        <?php   
         
        return ob_get_clean();
    }

}

//init
new VillaSearchShortcode();