<?php
/**
 * Featured Destinations
 *
 *
 * This vc element will show selected destinations
 * with title, featured_image, brief and link to
 * it's property list.
 * 
 */
class FeaturedDestinationsShortcode extends WPBakeryShortCode{

	// Element Init
    function __construct() {
        add_action( 'init', array( $this, 'featured_destinations_shortcode_mapping' ) );
        add_shortcode( 'featured_destinations_shortcode', array( $this, 'featured_destinations_shortcode_output' ) );
    }

    // Element Mapping to VC
    public function featured_destinations_shortcode_mapping(){

    	// Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }

        // Map the block with vc_map()
        vc_map( 
            array(
                'name' => __('Featured Destinations', 'waynakh-theme'),
                'base' => 'featured_destinations_shortcode',
                'description' => __('A slideshow of featured destinations.', 'waynakh-theme'), 
                'category' => __('Waynakh Theme Shortcodes', 'waynakh-theme'),   
                'icon' => get_stylesheet_directory_uri().'/assets/images/vc-icon-villa.png',          
                'params' => array(   
                         
                    array(
                        'type' => 'textfield',
                        'holder' => 'h3',
                        'class' => 'title-class',
                        'heading' => __( 'Number of destinations.', 'waynakh-theme' ),
                        'param_name' => 'number_of_destinations',
                        'value' => __( '6', 'waynakh-theme' ),
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Custom Group',
                    )                     
                        
                ),
            )
        ); 
    }

    // Output Shortcode Element
    public function featured_destinations_shortcode_output( $atts ){

    	// Params extraction
        extract(
            shortcode_atts(
                array(
                    'number_of_destinations'   => ''
                ), 
                $atts
            )
        ); 

        ob_start(); ?>

        <div class="featured-destinations">

        	<?php
        		$taxonomies = array('property_city');
        		$featured_destinations = get_terms($taxonomies);

        		//var_dump($featured_destinations);
			?>

			<?php 
				foreach ($featured_destinations as $destination) {

					$title = $destination->name;
					$image = get_term_meta($destination->term_id, 'wpcf-term_featured_image', true);

                    $term_description = get_term_meta( $destination->term_id, 'wpcf-term_description', true );

                    $description_short = wp_trim_words($term_description, 30, '...');

					$url = get_term_link($destination->term_id, 'property_city'); 
				?>

				<div class="featured-destinations__destination">
					<div class="featured-destinations__destination-inner">
						<h3 class="__title"><?php echo $title; ?></h3>
						<img src="<?php echo $image ?>"/>

						<div class="__description">
							<div>
                                <?php 
                                    echo $description_short; 
                                ?>
                            </div>
							<a class="__read-more" href="<?php echo $url; ?>">
								<?php echo sprintf('All %s Villas', $title); ?>
							</a>
						</div>
					</div>
				</div>
				<?php
				}
			?>		

        </div><!--/.featured-destinations-->
        <div class="featured-destinations__slider-controls">
            <a href="javascript:;" class="prev">Prev</a>
            <a href="javascript:;" class="next">Next</a>
        </div> 

        <?php   
         
        return ob_get_clean();

    }

}

//init
new FeaturedDestinationsShortcode();