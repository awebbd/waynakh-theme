<?php
/**
 * Featured Categories
 *
 *
 *
 * Shows a horizontally aligned list of property categories
 * with their name, link and icon selected while
 * editing the category.
 */

class FeaturedCategoriesShortcode extends WPBakeryShortCode{

	// Element Init
    function __construct() {
        add_action( 'init', array( $this, 'featured_categories_shortcode_mapping' ) );
        add_shortcode( 'featured_categories_shortcode', array( $this, 'featured_categories_shortcode_output' ) );
    }

    // Element Mapping to VC
    public function featured_categories_shortcode_mapping(){

    	// Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }

        // Map the block with vc_map()
        vc_map( 
            array(
                'name' => __('Featured Categories', 'waynakh-theme'),
                'base' => 'featured_categories_shortcode',
                'description' => __('A horizontal grid of featured categories.', 'waynakh-theme'), 
                'category' => __('Waynakh Theme Shortcodes', 'waynakh-theme'),   
                'icon' => get_stylesheet_directory_uri().'/assets/images/vc-icon-villa.png',          
                'params' => array(   
                         
                    
                    array(
                    	'type' => 'checkbox',
                    	'holder' => 'h3',
                        'class' => 'title-class',
                        'heading' => __( 'Select categories to show.', 'waynakh-theme' ),
                        'param_name' => 'selected_categories',
                        'value' =>  aweb_get_term_ids('property_category'),
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Custom Group',


                    )                     
                        
                ),
            )
        ); 
    }

    /**
     * Outputs the shortcode 
     *
     *
     * 
     */
    public function featured_categories_shortcode_output( $atts ){
    	// Params extraction
        extract(
            shortcode_atts(
                array(
                    'selected_categories' => ''
                ), 
                $atts
            )
        ); 
        ob_start(); ?>

        <div class="featured-categories clearfix">

        	<?php

        		$args = array(
        			'taxonomy' => 'property_category',
        			'hide_empty' => false,
        			'include' => explode(",", $selected_categories)
        		);
        		$categories = get_terms($args);	
        	?>

        	<?php foreach( $categories as $category ) :?>

        		<?php
        			//valuse
        			$title = $category->name;
        			$icon_name = get_term_meta($category->term_id, 'wpcf-property-category-icon-name', true);
        			$url = get_term_link($category->term_id, 'property_category');
        		?>

        		<a class="featured-categories__category" href="<?php echo $url; ?>">
        			<span class="icon <?php echo $icon_name; ?>"></span>
        			<h3 class="category-name">
        				<?php echo $category->name; ?>     					
        			</h3>
        		</a>

        	<?php endforeach; ?>       	

        </div>       	

        <?php   
         
        return ob_get_clean();
    }


}
//init
new FeaturedCategoriesShortcode();