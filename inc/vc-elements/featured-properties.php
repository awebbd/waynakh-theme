<?php
/**
 * Featured Properties
 *
 * This vc element shows featured properties in
 * a custom grid view.
 *
 * @since  1.0.0
 * @package  waynakh-theme/inc/vc-elements
 * 
 */

class FeaturedPropertiesShortcode extends WPBakeryShortCode{

	// Element Init
    function __construct() {
        add_action( 'init', array( $this, 'featured_properties_shortcode_mapping' ) );
        add_shortcode( 'featured_properties_shortcode', array( $this, 'featured_properties_shortcode_output' ) );
    }

    // Element Mapping to VC
    public function featured_properties_shortcode_mapping(){

    	// Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }

        // Map the block with vc_map()
        vc_map( 
            array(
                'name' => __('Featured Properties', 'waynakh-theme'),
                'base' => 'featured_properties_shortcode',
                'description' => __('A grid view of featured properties.', 'waynakh-theme'), 
                'category' => __('Waynakh Theme Shortcodes', 'waynakh-theme'),   
                'icon' => get_stylesheet_directory_uri().'/assets/images/vc-icon-villa.png',          
                'params' => array(   
                         
                    array(
                        'type' => 'textfield',
                        'holder' => 'h3',
                        'class' => 'title-class',
                        'heading' => __( 'Number of properties.', 'waynakh-theme' ),
                        'param_name' => 'number_of_properties',
                        'value' => __( '6', 'waynakh-theme' ),
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Custom Group',
                    )                     
                        
                ),
            )
        ); 
    }

    // Output Shortcode Element
    public function featured_properties_shortcode_output( $atts ){

    	// Params extraction
        extract(
            shortcode_atts(
                array(
                    'number_of_properties'   => ''
                ), 
                $atts
            )
        ); 

        ob_start(); ?>

        <div class="featured-properties block-grid-xs-1 block-grid-md-3">

        	<?php
			$args = array(
			        'post_type' => 'estate_property',
			        'meta_key'     => 'prop_featured',
			        'meta_value' => 1,
			        'meta_compare' => '=',
			        'posts_per_page' => $number_of_properties
			    );
			$featured_properties = new WP_Query( $args ); ?>

			<?php while ( $featured_properties->have_posts() ) : $featured_properties->the_post(); ?>

            	<?php get_template_part('template-parts/vc-views/featured', 'property') ?>

       	 	<?php endwhile; ?>
         
        </div><!--/.featured-properties-->

        <?php   
         
        return ob_get_clean();

    }
}

// Element Class Init
new FeaturedPropertiesShortcode();   
