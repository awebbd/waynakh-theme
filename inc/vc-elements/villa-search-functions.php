<?php
/**
 * Villa Search Functions
 *
 * 
 */
add_action('wp_ajax_villa_search_load_suggestions', 'villa_search_load_suggestions');
add_action('wp_ajax_nopriv_villa_search_load_suggestions', 'villa_search_load_suggestions');

function villa_search_load_suggestions(){

	//data sent from js
	$input_text = $_POST['inputText'];

	//query cities
	if($input_text){
		$args = array(
			'taxonomy' => array('property_city'),
			'search' => $input_text,
			'number' => 3
		);
	}else{
		$args = array(
			'taxonomy' => array('property_city'),
			'number' => 3
		);
	}
	

	$cities = get_terms($args);

	?>

	<?php if ( ! empty( $cities ) && ! is_wp_error( $cities ) ) : ?>

		<?php foreach ($cities as $city) : ?>

		<a class="__suggestion clearfix">
			<div class="__suggestion-location">
				<p class="__name"><?php echo $city->name; ?></p>
			</div>
			<div class="__suggestion-villa-count">
				<div class="btn btn--rounded">
					<?php echo $city->count; ?> Villa
				</div>
			</div>
		</a>

		<?php endforeach; ?>

	<?php else: ?>

		<small>Nothings found.</small>

	<?php endif; ?>

	<?php

	// Always die in functions echoing Ajax content
    die();
}