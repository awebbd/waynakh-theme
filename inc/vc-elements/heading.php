<?php
/**
 * Heading
 *
 * Shows a custom title and optional subtitle.
 *
 * @package waynakh-theme\vc-elements\
 */
class HeadingShortcode extends WPBakeryShortCode{

	// Element Init
    function __construct() {
        add_action( 'init', array( $this, 'heading_shortcode_mapping' ) );
        add_shortcode( 'heading_shortcode', array( $this, 'heading_shortcode_output' ) );
    }

    // Element Mapping to VC
    public function heading_shortcode_mapping(){

    	// Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }

        // Map the block with vc_map()
        vc_map( 
            array(
                'name' => __('Heading', 'waynakh-theme'),
                'base' => 'heading_shortcode',
                'description' => __('A title and optional sub-title.', 'waynakh-theme'), 
                'category' => __('Waynakh Theme Shortcodes', 'waynakh-theme'),   
                'icon' => get_stylesheet_directory_uri().'/assets/images/vc-icon-villa.png',          
                'params' => array(   
                         
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'title-class',
                        'heading' => __( 'Title', 'waynakh-theme' ),
                        'param_name' => 'title',
                        'value' => __( 'Your desired title.', 'waynakh-theme' ),
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Custom Group',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h3',
                        'class' => 'title-class',
                        'heading' => __( 'Sub Title', 'waynakh-theme' ),
                        'param_name' => 'sub_title',
                        'value' => __( 'Your subtitle goes here.', 'waynakh-theme' ),
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Custom Group',
                    )                     
                        
                ),
            )
        ); 
    }

    // Output Shortcode Element
    public function heading_shortcode_output( $atts ){

    	// Params extraction
        extract(
            shortcode_atts(
                array(
                    'title'   => '',
                    'sub_title'   => ''
                ), 
                $atts
            )
        ); 

        ob_start(); ?>

        <div class="heading">
        	<h2 class="heading__title"><?php echo $title; ?></h2>
        	<p class="heading__sub-title"><?php echo $sub_title; ?></p>
        </div><!--/.heading-->

        <?php   
         
        return ob_get_clean();

    }


}

//now init shortcode class
new HeadingShortcode();