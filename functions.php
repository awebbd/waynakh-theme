<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;


/**
 * Setup of theme and some default settings
 * propagated
 *
 * 
 */
function waynakh_theme_setup() {

    //Loads themes text domain for translation.
    load_child_theme_textdomain( 'waynakh-theme', get_stylesheet_directory() . '/languages' );

    /*
     * Enable support for Post Thumbnails on posts and pages.
     *
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support( 'post-thumbnails' );

    /**
     * Adding image sizes
     */
    add_image_size( 'property-grid-thumb', 364, 200, true );
    add_image_size( 'property-nav-thumb', 200, 150, true );
}
add_action( 'after_setup_theme', 'waynakh_theme_setup' );





/**
 * Parent theme
 */
        
if ( !function_exists( 'chld_thm_cfg_parent_css' ) ):
    function chld_thm_cfg_parent_css() {
        wp_enqueue_style('wpestate_bootstrap',get_template_directory_uri().'/css/bootstrap.css', array(), '1.0', 'all');
        wp_enqueue_style('wpestate_bootstrap-theme',get_template_directory_uri().'/css/bootstrap-theme.css', array(), '1.0', 'all');
        wp_enqueue_style( 'chld_thm_cfg_parent', trailingslashit( get_template_directory_uri() ) . 'style.css' ); 
        wp_enqueue_style('wpestate_media',get_template_directory_uri().'/css/my_media.css', array(), '1.0', 'all'); 


        //remove parent controls-js and add child themes
        wp_dequeue_script( 'wpestate_control' );
        wp_enqueue_script('wpestate_control', trailingslashit( get_stylesheet_directory_uri() ).'assets/js/wprentals/control.js',array('jquery'), '1.0', true); 
    }
endif;
add_action( 'wp_enqueue_scripts', 'chld_thm_cfg_parent_css' );


/**
 * Enqueue scripts and styles.
 */
function waynakh_theme_scripts() {
    //stylesheets
	wp_enqueue_style( 'waynakh-theme-style', get_stylesheet_directory_uri(). '/assets/css/style.min.css', array(), '201243206', 'all' );

    //fonts
    wp_enqueue_style( 'waynakh-theme-font-pt-sans', 'https://fonts.googleapis.com/css?family=PT+Sans:700', array('waynakh-theme-style'), '565026026', 'all');
    

    //javascript
    wp_register_script( 'waynakh-theme-scripts', 
        get_stylesheet_directory_uri() . '/assets/js/scripts.min.js', 
        array(
            'jquery', 
            'waynakh-theme-slick-slider-js',
            'jquery-ui-datepicker'
        ), 
        '20120206', 
        true );

    $waynakh_theme_localized_data_array = array(
        'themeDir' => get_template_directory_uri(),
        'childThemeDir' => get_stylesheet_directory_uri(),
        'ajaxurl' => admin_url( 'admin-ajax.php' )
    );

    wp_localize_script( 'waynakh-theme-scripts', 'waynakh_theme_localized_data', $waynakh_theme_localized_data_array );
    wp_enqueue_script( 'waynakh-theme-scripts' );

    //slick
    wp_enqueue_script( 'waynakh-theme-slick-slider-js', get_stylesheet_directory_uri() . '/assets/js/vendor/slick.min.js', array('jquery'), '25170206', true );
    //matchheight
    wp_enqueue_script( 'waynakh-theme-match-height-js', get_stylesheet_directory_uri() . '/assets/js/vendor/jquery.matchHeight-min.js', array('jquery'), '251630206', true );




}
add_action( 'wp_enqueue_scripts', 'waynakh_theme_scripts' );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function waynakh_theme_widgets_init() {
    register_sidebar( array(
        'name'          => esc_html__( 'Pre-Footer Sidebar', 'faces-theme' ),
        'id'            => 'pre-footer-sidebar',
        'description'   => 'This will show up in pre-footer section. Please add five blocks here.',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4 class="widgettitle">',
        'after_title'   => '</h4>',
    ) );
    register_sidebar( array(
        'name'          => esc_html__( 'Footer Newsletter Sidebar', 'faces-theme' ),
        'id'            => 'footer-newsletter-sidebar',
        'description'   => '',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4 class="widgettitle">',
        'after_title'   => '</h4>',
    ) );
    register_sidebar( array(
        'name'          => esc_html__( 'Footer Bottom Right Sidebar', 'faces-theme' ),
        'id'            => 'footer-bottom-right-sidebar',
        'description'   => '',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4 class="widgettitle">',
        'after_title'   => '</h4>',
    ) );

    register_sidebar(array(
        'name' => esc_html__( 'Fourth Footer Widget Area', 'wpestate'),
        'id' => 'fourth-footer-widget-area',
        'description' => esc_html__( 'The Fourth footer widget area', 'wpestate'),
        'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</li>',
        'before_title' => '<h3 class="widget-title-footer">',
        'after_title' => '</h3>',
    ));
}
add_action( 'widgets_init', 'waynakh_theme_widgets_init' );


/**
 * aweb_get_term_ids
 *
 *
 * @link http://woocommerce.wp-a2z.org/oik_api/wc_data_store_wpget_term_ids/
 */
function aweb_get_term_ids( $taxonomy ) {
    
    $terms = get_terms( $taxonomy, array('hide_empty'=> false) );
    if ( false === $terms || is_wp_error( $terms ) ) {
      return array();
    }
    return wp_list_pluck( $terms, 'term_id', 'name' );
}

/**
 * Includes utility functions
 */
require get_stylesheet_directory(). '/inc/utility-functions.php';


/**
 * Including custom vs elements
 *
 *
 * 
 */
require get_stylesheet_directory(). '/inc/vc-elements/featured-properties.php';
require get_stylesheet_directory(). '/inc/vc-elements/featured-destinations.php';

require get_stylesheet_directory(). '/inc/vc-elements/featured-categories.php';

require get_stylesheet_directory(). '/inc/vc-elements/heading.php';


require get_stylesheet_directory(). '/inc/vc-elements/villa-search.php';
require get_stylesheet_directory(). '/inc/vc-elements/villa-search-functions.php';


/**
 * Including custom widgets
 */
require get_stylesheet_directory(). '/inc/widgets/widget-icon-link.php';

require get_stylesheet_directory(). '/libs/waynakh_help_functions.php';