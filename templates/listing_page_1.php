<?php
global $post;
global $current_user;
global $feature_list_array;
global $propid ;
global $post_attachments;
global $options;
global $where_currency;
global $property_description_text;     
global $property_details_text;
global $property_features_text;
global $property_adr_text;  
global $property_price_text;   
global $property_pictures_text;    
global $propid;
global $gmap_lat;  
global $gmap_long;
global $unit;
global $currency;
global $use_floor_plans;
global $favorite_text;
global $favorite_class;
global $property_action_terms_icon;
global $property_action;
global $property_category_terms_icon;
global $property_category;
global $guests;
global $bedrooms;
global $bathrooms;
global $show_sim_two;
global $guest_list;
global $post_id;

$price              =   intval   ( get_post_meta($post->ID, 'property_price', true) );
$price_label        =   esc_html ( get_post_meta($post->ID, 'property_label', true) );  
$property_city      =   get_the_term_list($post->ID, 'property_city', '', ', ', '') ;
$property_area      =   get_the_term_list($post->ID, 'property_area', '', ', ', '');

$post_id=$post->ID; 
$guest_no_prop ='';
if(isset($_GET['guest_no_prop'])){
    $guest_no_prop = intval($_GET['guest_no_prop']);
}
$guest_list= wpestate_get_guest_dropdown('noany');
?>
<div class="row content-fixed-listing listing_type_1">

    <?php 
        //loop starts
        while (have_posts()) : the_post();
    ?>

    <!--listing-pre-header starts-->
    <div class="listing-pre-header col-md-12">
        <div class="row">
            <div class="col-sm-6">
                <?php get_template_part('templates/breadcrumbs'); ?>
            </div>
            <div id="social-share--desktop" class="col-sm-6">
                <?php get_template_part('templates/social_share_icons'); ?>
            </div>
        </div>
    </div>
    <!--listing-pre-header ends-->

    <!-- .listing-header starts-->    
    <div class="listing-header col-md-12">
        <div class="box box--bg-dark clearfix">
            <div class="row">            
                <div class="col-md-8 listing-slider-container">
                    <?php  
                    get_template_part('templates/listing-slider');
                    ?>
                </div>   
                <div class="col-md-4">
                    <?php  
                    get_template_part('templates/listing-info');
                    ?>
                </div>         
            </div>
            <div class="row">
                <div class="col-md-12 listing-slider-nav-container">
                    <?php  
                    get_template_part('templates/listing-slider-nav');
                    ?>
                </div>
            </div>
        </div>
    </div>   
    <!--.lisging-header ends--> 

    <div class="col-md-12">
        <div id="social-share--mobile" class="col-md-6">
            <?php get_template_part('templates/social_share_icons'); ?>
        </div>
    </div>

    <!--.listing-rates and .listing-reservation starts--> 
    <div class="col-md-12">
        <div class="box box--bg-dark">        
            <div class="row">
                <div class="col-md-8">
                    <div class="box match-height-row-1">
                    <?php 
                        get_template_part ('templates/property-rates');
                    ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div id="property-booking" class="box match-height-row-1">
                    <?php 
                        get_template_part ('templates/booking_form_template'); 
                    ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--.listing-rates and .listing-reservation ends--> 

    <!--.listing-amenities and .listing-services starts--> 
    <div class="col-md-12">
        <div class="box box--bg-dark">
            <div class="row">            
                <div class="col-md-8">        
                    <?php 
                        get_template_part ('templates/listing-amenities');
                    ?>
                </div>
                
                <div class="col-md-4">
                    <?php 
                        get_template_part ('templates/listing-services'); 
                    ?>
                </div>
            </div>
        </div> 
    </div>
    <!--.listing-amenities and .listing-services ends-->

    <!-- .listing-description starts-->
    <div class="col-md-12">
        <div class="box box--bg-dark">
            <div class="listing-description box">
                <div class="__heading">
                   <h2>Villa Aciklamasi</h2>
                   <p>Villanin tum aciklamasi burada</p>
                </div>
                <?php
                    $content = get_the_content();
                    $content = apply_filters('the_content', $content);
                    $content = str_replace(']]>', ']]&gt;', $content);
                    if($content!=''){   
                           
                        $property_description_text =  get_option('wp_estate_property_description_text');
                        if (function_exists('icl_translate') ){
                            $property_description_text     =   icl_translate('wpestate','wp_estate_property_description_text', esc_html( get_option('wp_estate_property_description_text') ) );
                        }
                        print '<div class="__content">'.$content.'</div>';       
                    }
                ?>
            </div>
        </div>
    </div>
    <!-- .listing-description ends-->

    <!--.listing-video .listing-map - starts-->
    <div id="listing-video-and-map" class="col-md-12">
        <div class="box box--bg-dark">
            <div class="row row-eq-height">
                <div class="col-md-6">
                    <div class="box">
                        <?php 
                            get_template_part ('templates/listing-video'); 
                        ?>   
                    </div>             
                </div>
                <div class="col-md-6">
                    <div id="listing-map" class="box clearfix">
                        <div class="__heading">
                            <h2>Villa Konumu</h2>
                            <p>Villanin tum aciklamasi burada</p>
                        </div>
                        <div class="__content google_map_on_list_wrapper">        
                            <div id="gmapzoomplus"></div>
                            <div id="gmapzoomminus"></div>
                            <div id="gmapstreet"></div>
                            <?php echo wpestate_show_poi_onmap();?>
                            
                            <div id="google_map_on_list" 
                                data-cur_lat="<?php   echo $gmap_lat;?>" 
                                data-cur_long="<?php echo $gmap_long ?>" 
                                data-post_id="<?php echo $post->ID; ?>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--.listing-video .listing-map - ends-->

    <!--#reservation-calender starts-->
    <div id="reservation-calender" class="col-md-12">
        <div class="box box--bg-dark">
            <div class="box clearfix">
                <div class="__heading">
                    <h2>Rezervasyon Takvimi</h2>
                    <p>Villanin tum aciklamasi burada</p>
                </div>
                <div class="__content">
                    <?php
                    get_template_part ('templates/show_avalability');
                    ?>  
                </div>
            </div>
        </div>
    </div>
    <!--#reservation-calender ends-->

    <!--reviews starts-->
    <div id="listing-reviews" class="col-md-12">
        <div class="box box--bg-dark">
            <div class="box clearfix">
                <div class="__heading">
                    <h2>Villa Hakkinda Yorumlar</h2>
                    <p>Villa Hakkinda Yorumlar</p>
                </div>
                <div class="__content">
                    <?php //get_template_part ('templates/listing_reviews'); ?>
                    <?php comments_template( '', true ); ?>
                </div>
            </div> 
        </div>   
    </div>
    <!--reviews ends-->

    <?php
        //wp_reset_query();
        endwhile; // end of the loop
        $show_compare=1;
    ?>
</div>   

<?php get_footer(); ?>