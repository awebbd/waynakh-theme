<?php
/**
 * Listing slider
 *
 * This template shows a slider of images listed
 * on its perticular listing page.
 *
 */





$arguments      = array(
                    'numberposts' => -1,
                    'post_type' => 'attachment',
                    'post_mime_type' => 'image',
                    'post_parent' => $post->ID,
                    'post_status' => null,
                    'exclude' => get_post_thumbnail_id(),
                    'orderby'         => 'menu_order',
                    'order'           => 'ASC'
                );

$post_attachments   = get_posts($arguments);
?>




<div class="listing-slider">
	<?php foreach ($post_attachments as $attachment){
			$full_img = wp_get_attachment_image_src( $attachment->ID, 'listing_full_slider');
			$full_prty = wp_get_attachment_image_src($attachment->ID, 'full');
			$attachment_meta    = wp_get_attachment($attachment->ID);
	?>

	<div class="listing-slider__slide">
		<img  src="<?php echo $full_img[0]; ?>" data-original="<?php echo $full_prty[0] ?>" alt="<?php echo $attachment_meta['alt']; ?>" class="img-responsive" />
	</div>

	<?php
	
	}

	?>	
</div>