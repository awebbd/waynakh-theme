<?php
/**
 * Listing Services
 */
$id = get_the_ID();
$electricity = get_post_meta( $id, 'electricity', true );
$water = get_post_meta( $id, 'water', true );
$gas = get_post_meta( $id, 'gas', true );
$swimming_pool = get_post_meta( $id, 'swimming-pool', true );
$garden = get_post_meta( $id, 'garden', true );
$cleaning = get_post_meta( $id, 'cleaning', true );
?>
<div id="listing-services" class="box match-height-row-2">
	
	<div class="__heading">
		<h2><?php _e('Services Including Liability', 'waynakh-theme'); ?></h2>
		<p><?php _e('The following services are included in the price list', 'waynakh-theme'); ?></p>
	</div>
	<ul class="clearfix">

		<?php if($electricity=='Yes'): ?>
		<li class="electricity">
			<div class="__inner">
				<p><?php _e('Electricity', 'waynakh-theme'); ?></p>
			</div>		
		</li>
		<?php endif; ?>

		<?php if($water=='Yes'): ?>
		<li class="water">
			<div class="__inner">
				<p><?php _e('Water', 'waynakh-theme'); ?></p>
			</div>				
		</li>
		<?php endif; ?>

		<?php if($gas=='Yes'): ?>
		<li class="gas">
			<div class="__inner">
				<p><?php _e('Gas', 'waynakh-theme'); ?></p>
			</div>
		</li>
		<?php endif; ?>

		<?php if($swimming_pool=='Yes'): ?>
		<li class="swimming-pool">
			<div class="__inner">
			<p><?php _e('Swimming Pool', 'waynakh-theme'); ?></p>
			</div>
		</li>
		<?php endif; ?>

		<?php if($garden=='Yes'): ?>
		<li class="garden">
			<div class="__inner">
			<p><?php _e('Garden', 'waynakh-theme'); ?></p>
			</div>
		</li>
		<?php endif; ?>

		<?php if($cleaning=='Yes'): ?>
		<li class="cleaning">
			<div class="__inner">
			<p><?php _e('Cleaning', 'waynakh-theme'); ?></p>
			</div>
		</li>
		<?php endif; ?>

	</ul>
	
</div>