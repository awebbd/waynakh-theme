<?php
/**
 * Listing Video
 *
 *
 * 
 */
$video_id = esc_html( get_post_meta($post->ID, 'embed_video_id', true) );
$video_type = esc_html( get_post_meta($post->ID, 'embed_video_type', true) );
?>
<?php if($video_id): ?>
<div id="listing-video">
    <div class="__heading">
        <h2>Villa Videosu</h2>
        <p>Villanin tum aciklamasi burada</p>
    </div>
    <div class="__content">
        <div class="responsive-video-container">
        	<?php
    			if($video_type=='vimeo'){
                    echo wpestate_custom_vimdeo_video($video_id);
                 }else{
                     echo wpestate_custom_youtube_video($video_id);
                 }
            ?>
        </div>
    </div>
</div>
<?php endif; ?>