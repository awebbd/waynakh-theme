<?php
/**
 * Listing slider Nav
 *
 * This template shows a slider of images listed
 * on its perticular listing page.
 *
 */
$arguments      = array(
                    'numberposts' => -1,
                    'post_type' => 'attachment',
                    'post_mime_type' => 'image',
                    'post_parent' => $post->ID,
                    'post_status' => null,
                    'exclude' => get_post_thumbnail_id(),
                    'orderby'         => 'menu_order',
                    'order'           => 'ASC'
                );

$post_attachments   = get_posts($arguments);
?>




<div class="listing-slider-nav">
	<?php foreach ($post_attachments as $attachment){
			$thumb_img = wp_get_attachment_image_src( $attachment->ID, 'property-nav-thumb');
			$attachment_meta    = wp_get_attachment($attachment->ID);
	?>

	<div class="listing-slider-nav__slide">
		<img  src="<?php echo $thumb_img[0]; ?>" alt="<?php echo $attachment_meta['alt']; ?>" class="img-responsive" />
	</div>

	<?php
	
	}

	?>	
</div>