<?php
/**
 * Listing Info
 *
 * This template shows main info of the listing
 * and 
 *
 */
$id = get_the_ID();
$guests = get_post_meta( $id, 'guest_no', true );
$bedrooms = get_post_meta( $id, 'property_bedrooms', true );
$bathrooms = get_post_meta( $id, 'property_bathrooms', true );
$price = get_post_meta( $id, 'property_price', true );
$aweb = get_post_meta( $id, 'aweb-test-field', true );
$comments_number = get_comments_number($id);
$property_rating = waynakh_property_rating_number($id);
//distances
$distance_from_market = get_post_meta( $id, 'distance-from-market', true );
$distance_from_sea = get_post_meta( $id, 'distance-from-sea', true );
$distance_from_bus = get_post_meta( $id, 'distance-from-bus', true );
$distance_from_restaurant = get_post_meta( $id, 'distance-from-restaurant', true );
$distance_from_city_center = get_post_meta( $id, 'distance-from-city-center', true );
$distance_from_air_port = get_post_meta( $id, 'distance-from-air-port', true );
$distance_from_hospital = get_post_meta( $id, 'distance-from-hospital', true );
?>
<div class="listing-info">	
	<div class="__heading">	
		<h1><?php the_title(); ?></h1>
	</div>

	<div class="__content">

		<ul class="__meta clearfix">
			<li class="guests">
				<div class="__inner">
					<?php 
						echo $guests.' ';
						_e('Guests', 'waynakh-theme');
					?>
				</div>
			</li>
			<li class="bedrooms">
				<div class="__inner">
					<?php 
						echo $bedrooms.' ';
						_e('Bedrooms', 'waynakh-theme');
					?>
				</div>
			</li>
			<li class="bathrooms">		
				<div class="__inner">
					<?php 
						echo $bathrooms.' ';
						_e('Bathrooms', 'waynakh-theme');
					?>
				</div>
			</li>
		</ul>
		
		<ul class="__distances">
			<li>
				<span class="market"><?php _e('Distance To Market', 'waynakh-theme'); ?></span>
				<span><?php echo $distance_from_market; ?></span>
			</li>
			<li>
				<span class="sea"><?php _e('Distance To Sea', 'waynakh-theme'); ?></span>
				<span><?php echo $distance_from_sea; ?></span>
			</li>
			<li>
				<span  class="bus"><?php _e('Distance To Bus', 'waynakh-theme'); ?></span>
				<span><?php echo $distance_from_bus; ?></span>
			</li>
			<li><span  class="restaurant"><?php _e('Restaurant', 'waynakh-theme'); ?></span><span><?php echo $distance_from_restaurant; ?></span>
			</li>
			<li><span  class="city-center"><?php esc_html_e( 'City Center', 'waynakh-theme' ) ?></span><span><?php echo $distance_from_city_center; ?></span>
			</li>
			<li>
				<span  class="air-port"><?php _e('Air Port', 'waynakh-theme'); ?></span> 
				<span><?php echo $distance_from_air_port; ?></span>
			</li>
			<li>
				<span  class="hospital"><?php _e('Hospital', 'waynakh-theme'); ?></span> 
				<span><?php echo $distance_from_hospital; ?></span>
			</li>
		</ul>
		<div class="__footer clearfix">
			<div class="rating">
				<span class="text">Puan</span>
				<span class="number"><?php echo $property_rating; ?></span>
			</div>

			<div class="comments">
				<span class="text">Yobum</span>
				<span class="number"><?php echo $comments_number; ?></span>
			</div>
			
			<div class="price">
				<span class="number"><?php echo $price; ?></span>
				<span class="currency">TL</span>
				<span class="note"><?php _e('Minimum nighly price.', 'waynakh-theme'); ?></span>
			</div>
		</div>
	</div>	
</div>




