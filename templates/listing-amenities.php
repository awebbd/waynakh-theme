<?php
/**
 * Listing Amenities
 */
global $feature_list_array;
?>
<div class="listing-amenities box match-height-row-2">
	<?php 
    	if ( count( $feature_list_array )!=0 && !count( $feature_list_array )!=1 ){ //  if are features and ammenties        
    ?>

    <div class="__heading">
    	<h2>Villa Ozellikleri</h2>
    	<p>Villanin tum ozellikleri burada</p>
    </div>

    <div class="__content">
        <div class="row">
    	   <?php print waynakh_estate_listing_features($post->ID); ?>
        </div>
    </div>   

    <?php
    } // end if are features and ammenties
    ?>
</div>