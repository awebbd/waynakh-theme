<?php
global $curent_fav;
global $currency;
global $where_currency;
global $show_compare;
global $show_compare_only;
global $show_remove_fav;
global $options;
global $isdashabord;
global $align;
global $align_class;
global $is_shortcode;
global $is_widget;
global $row_number_col;
global $full_page;
global $listing_type;
global $property_unit_slider;
global $book_from;
global $book_to;
global $guest_no;

$pinterest          =   '';
$previe             =   '';
$compare            =   '';
$extra              =   '';
$property_size      =   '';
$property_bathrooms =   '';
$property_rooms     =   '';
$measure_sys        =   '';

$col_class  =   'col-md-6';
$col_org    =   4;
 $title=get_the_title($post->ID);

if(isset($is_shortcode) && $is_shortcode==1 ){
    $col_class='col-md-'.$row_number_col.' shortcode-col';
}

if(isset($is_widget) && $is_widget==1 ){
    $col_class='col-md-12';
    $col_org    =   12;
}

if(isset($full_page) && $full_page==1 ){
    $col_class='col-md-4 ';
    $col_org    =   3;
    if(isset($is_shortcode) && $is_shortcode==1 && $row_number_col==''){
        $col_class='col-md-'.$row_number_col.' shortcode-col';
    }
}

$link           =  esc_url ( get_permalink());

if ( isset($_GET['check_in']) && isset($_GET['check_out']) ){
    $check_out= sanitize_text_field ( $_GET['check_out'] );
    $check_in= sanitize_text_field ( $_GET['check_in'] );
 
    $link   =   add_query_arg( 'check_in_prop', $check_in, $link);
    $link   =   add_query_arg( 'check_out_prop', $check_out, $link);
    
   
    if(isset($_GET['guest_no'])){
        $guest_no=intval($_GET['guest_no']);
        $link=add_query_arg( 'guest_no_prop', $guest_no, $link);
    }
}else{
    if ($book_from!='' && $book_to!=''){
        $book_from  = sanitize_text_field ($book_from);
        $book_to    = sanitize_text_field ( $book_to );
        $link   =   add_query_arg( 'check_in_prop', $book_from, $link);
        $link   =   add_query_arg( 'check_out_prop', $book_to, $link);
    
        if($guest_no!=''){
            $link=add_query_arg( 'guest_no_prop', intval($guest_no), $link);
        }
        
    }
}





$preview        =   array();
$preview[0]     =   '';
$favorite_class =   'icon-fav-off';
$fav_mes        =   esc_html__( 'add to favorites','wpestate');
if($curent_fav){
    if ( in_array ($post->ID,$curent_fav) ){
    $favorite_class =   'icon-fav-on';   
    $fav_mes        =   esc_html__( 'remove from favorites','wpestate');
    } 
}

$listing_type_class='property_unit_v2';
if($listing_type==1){
    $listing_type_class='';
} 
$property_status= stripslashes ( get_post_meta($post->ID, 'property_status', true));


?>


<?php
//all values
$id = get_the_ID();
$name = get_the_title();
$city = wp_get_post_terms($id, 'property_city', array("fields" => "all"))[0];
$city_url = get_term_link($city->term_id);
$area = wp_get_post_terms($id, 'property_area', array("fields" => "all"))[0];
$area_url = get_term_link($area->term_id);
$guests = get_post_meta( $id, 'guest_no', true );
$bedrooms = get_post_meta( $id, 'property_bedrooms', true );
$bathrooms = get_post_meta( $id, 'property_bathrooms', true );
$price = get_post_meta( $id, 'property_price', true );
$property_rating = waynakh_property_rating_number($id);
?>


<div class="property-listing-wrapper <?php echo $col_class.' '.$listing_type_class; ?> ssx property_flex " data-org="<?php echo $col_org;?>" data-listid="<?php echo $post->ID;?>" > 
    <div class="property-listing" data-link="<?php echo $link;?>">
        <header>
            <div class="property-listing__image">
                <a href="<?php the_permalink(); ?>">
                <?php
                    if(has_post_thumbnail()){
                        the_post_thumbnail( 'property-grid-thumb');
                    }
                ?>
                </a>
            </div>
            <div class="property-listing__heading">
                <h2><a href="<?php the_permalink(); ?>"><?php echo $name; ?></a></h2>
                <h3>
                    <a href="<?php echo $area_url; ?>"><?php echo $area->name; ?></a> | 

                    <a href="<?php echo $city_url; ?>"><?php echo $city->name; ?></a></h3>
            </div>
        </header>
        <section class="property-listing__property-info">
            <div class="row">
                <div class="property-listing__property-price column col-xs-4">
                    <span class="head"><?php _e('Night', 'waynakh-theme'); ?></span>
                    <span class="amount"><?php echo $price; ?>,</span>
                    <span class="amount-ext">00</span>
                    <span class="currency">TL</span>
                    <span class="note"><?php _e('Prices starting from', 'waynakh-theme'); ?></span>
                </div>

                <div class="property-listing__property-point column col-xs-2">
                    <span class="text"><?php _e('Point', 'waynakh-theme'); ?></span>
                    <span class="number">
                        <?php echo $property_rating; ?>
                    </span>
                </div>
            
                <div class="column col-xs-6 clearfix">
                    <div class="property-listing__property-guests">
                        <div class="inner">
                            <?php echo $guests; ?>
                        </div>
                    </div>
                    <div class="property-listing__property-bedrooms">
                        <div class="inner">
                            <?php echo $bedrooms; ?>
                        </div>
                    </div>
                    <div class="property-listing__property-bathrooms">
                        <div class="inner">
                            <?php echo $bathrooms; ?>
                        </div>
                    </div>
                </div>
            </div>          
        </section>
    </div>
</div>