(function ($, window, document, undefined) {

  'use strict';

  $(function () {

      /**
       * featured destination slider
       *
       * 
       */
  		$(document).ready(function(){
  			//destination slider
	        $('.featured-destinations').slick({
    				infinite: true,
    				slidesToShow: 4,
    				slidesToScroll: 4,
            arrows: false,
            dots: true,
            responsive: [
              {
                breakpoint: 720,
                settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1
                }
              }
          ]
	        });

          $('.featured-destinations__slider-controls .prev').click(function(){
              
              $('.featured-destinations').slick('slickPrev');
          });
          $('.featured-destinations__slider-controls .next').click(function(){
              
              $('.featured-destinations').slick('slickNext');
          });
  		});

      /**
       * number-input--with-button
       *
       *
       *
       * Number input with plus minus button
       * to add and deduct values.
       *
       * @link https://css-tricks.com/number-increment-buttons/ 
       */
      $(document).ready(function(){

        $('.input-number--with-buttons .btn').on('click', function(event){
          var $button = $(this);
          var oldValue = $button.parent().find('input').val();
          var newVal = '';

          if ($button.text() === '+') {
             newVal = parseFloat(oldValue) + 1;
          } else {
           // Don't allow decrementing below zero
            if (oldValue > 0) {
              newVal = parseFloat(oldValue) - 1;
            } else {
              newVal = 0;
            }
          }

          $button.parent().find('input').val(newVal);
        });
        
      });

      /**
       * Villa search
       */
      $(document).ready(function(){

        var advancedCity = $('#advanced_city');
        var suggestionBox = $('.advanced-city-suggestions');
        var suggestionLocation = $('.__suggestion-location');
        

        advancedCity.on('focus', function(){
          suggestionBox.fadeIn('slow');
        });

        advancedCity.on('focusout', function(){
          suggestionBox.fadeOut('slow');
        });

        
        
        /**
         * Ajax suggestions
         */
        $('#advanced_city').on('keypress', function(){
          
          $('.advanced-city-suggestions .__inner-wrap .__ajax-content').hide();

          $('.please-wait').show();

          var inputText = $(this).val();
          var wpAjaxURL = waynakh_theme_localized_data.ajaxurl;

          $.ajax({
            url : wpAjaxURL,
            type : 'post',
            data : {
              action : 'villa_search_load_suggestions',
              inputText : inputText
            },
            beforeSend : function(){
              $('.please-wait').show();
              $('.advanced-city-suggestions .__inner-wrap .__ajax-content').hide();
            },
            success : function(response){
              if(response !== null){
                $('.please-wait').hide();

                $('.advanced-city-suggestions .__inner-wrap .__ajax-content').html('');
                $('.advanced-city-suggestions .__inner-wrap .__ajax-content').html(response);
                $('.advanced-city-suggestions .__inner-wrap .__ajax-content').show();
                              
              }
              
            }
          });

        });//ajax ends


        /**
        var locationButton = $('.__suggestion-villa-count .btn');

        //get values and set in input
        locationButton.on('click', function(){
          var e = $(this);
          var locationSelected = e.parent().find('.__suggestion-location .__name').text();
          
          advancedCity.val(locationSelected);
        });
        **/



      });

      //click on location
      $(document).on("click", '.__suggestion-location .__name', function(event) {
          var advancedCity2 = $('#advanced_city');

          var btn = $(this);

          var locationSelected2 = btn.parent().parent().find('.__name').text();
          
          advancedCity2.val(locationSelected2);

      });
      //click on location villa count button
      $(document).on("click", '.__suggestion-villa-count .btn', function(event) {
          var advancedCity3 = $('#advanced_city');

          var btn = $(this);

          var locationSelected3 = btn.parent().parent().find('.__name').text();
          
          advancedCity3.val(locationSelected3);

      });



      /**
       * Listing Slider
       *
       * 
       */
      $(document).ready(function(){        
          $('.listing-slider').slick({
            infinite: true,
            arrows: true,
            dots: false,
            responsive: [
              {
                breakpoint: 576,
                settings: {
                  arrows: true,
                }
              }
            ]
          });
          $('.listing-slider-nav').slick({
            arrows: false,
            dots: false,
            slidesToShow: 7,
            slidesToScroll: 1,
            asNavFor: '.listing-slider',
            focusOnSelect: true,
            centerMode: false
          });
      });

      /**
       * Match Height For Listing Page
       * Blocks
       */
      $('.match-height-row-1').matchHeight();
      $('.match-height-row-2').matchHeight();
      $('.match-height-row-3').matchHeight();

      /**
       * Pre-footer images or logos
       *
       * they will be slider on mobile devices and
       * simple grid of images on others :)
       */
      $(document).ready(function(){
        $('#pre-footer--mobile .row').slick({
            infinite: true,
            arrows: true,
            dots: false,
            slidesToShow: 1,
            slidesToScroll: 1
        });
      });
      

  });

})(jQuery, window, document);
